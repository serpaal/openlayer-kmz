var map;
function init() {
    document.removeEventListener('DOMContentLoaded', init);
    map = new ol.Map({
        target: 'map',
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM()
            }),
            //vectorLayer
        ],
        controls: [
            //Define the default controls
            new ol.control.Zoom(),
            new ol.control.Rotate(),
            new ol.control.Attribution(),
            //Define some new controls
            new ol.control.ZoomSlider(),
            new ol.control.MousePosition(),
            new ol.control.ScaleLine(),
            new ol.control.OverviewMap()
        ],
        /*interactions: ol.interaction.defaults().extend([
            new ol.interaction.Select({
                layers: [vectorLayer]
            })
        ]),*/
        view: new ol.View({
            center: [0, 0],
            zoom: 2
        })
    });

    document.getElementById('files').addEventListener('change', function (ev) {
        var file =  ev.target.files[0];
        if(!file){
            return;
        }
        // Instanciando JsZip');
        var jsZip = new JSZip();
        jsZip.loadAsync(file)                                   // 1) read the Blob
            .then(function(zip) {
                zip.forEach(function (relativePath, zipEntry) {  // 2) print entries
                    /* JSZip extract file objects */
                    zip.files[zipEntry.name].async('blob').then(function (fileData){
                        /* Creamos un File con el contenido de blob. Retorna una promise */
                        return new File([fileData], zipEntry.name);
                    }).then(function (f) {
                        var reader = new FileReader();
                        reader.onload = function () {
                            var vector = new ol.layer.Vector({
                                source: new ol.source.Vector({
                                    url: reader.result,
                                    format: new ol.format.KML()
                                })
                            });
                           map.addLayer(vector);
                            vector.getSource().once('change', function(evt){
                                if (vector.getSource().getState() === 'ready') {
                                    var features = vector.getSource().getFeatures();
                                    console.dir(features);
                                   map.getView().fit(vector.getSource().getExtent(), map.getSize());
                                }
                            });
                        }
                        reader.readAsDataURL(f);
                    })
                })
            }, function (e) {
                console.dir(e);
            });
        console.log('Vector layer added successfully.');
        $('#files').val('');
    }, false);
}
document.addEventListener('DOMContentLoaded', init);
